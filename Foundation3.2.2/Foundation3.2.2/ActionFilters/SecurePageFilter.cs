﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using Foundation3._2._2.Authenticator;

namespace Foundation3._2._2.ActionFilters {
    public class SecurePageFilter : ActionFilterAttribute {
        public override void OnActionExecuting(ActionExecutingContext filterContext) {

            if (!SecurePage((Contact)filterContext.HttpContext.Session["Contact"], filterContext.RequestContext.HttpContext.Request.Cookies))
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "Controller", "Account" }, { "Action", "Index" } });
        }

        public static bool SecurePage(Contact contact, HttpCookieCollection cookies) {
            AuthenticatorClient authenticator = new AuthenticatorClient();

            try {
                if (!authenticator.ValidateToken(contact.Email, cookies["ACCESS_TOKEN"].Value)) return false;
            } catch (Exception ex) {
                return false;
            }

            return true;
        }
    }
}