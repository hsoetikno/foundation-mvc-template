﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using Foundation3._2._2.Authenticator;

namespace Foundation3._2._2.ActionFilters {
    public class AuthenticationFilter : ActionFilterAttribute {
        public override void OnActionExecuting(ActionExecutingContext filterContext) {
            if (SecurePageFilter.SecurePage((Contact)filterContext.HttpContext.Session["Contact"], filterContext.RequestContext.HttpContext.Request.Cookies))
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "Controller", "Main" }, { "Action", "Index" } });
        }
    }
}